import './styles/index.scss'
import directives from'./directives'
import components from'./components'
import 'virtual:svg-icons-register'

const plugin = {
  install (Vue) {
    Vue.config.globalProperties.$tours = {}

    Object.keys(components).forEach((key) => {
      const componentName = `Ok${key}`.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase()
      const component = components[key]
      Vue.component(componentName, component)
    })
    Object.values(directives).forEach((directive) => {
      directive(Vue)
    })
  },
}

export default plugin

import Ripple from './Ripple'
import Tooltip from './ToolTip'

export default { Ripple, Tooltip }

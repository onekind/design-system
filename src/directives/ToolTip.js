const tooltipDirective = (app) => {
  app.directive('ok-tooltip', {
    mounted(el, binding) {
      init(el, binding)
    },
    updated(el, binding) {
      init(el, binding)
    },
  })
}

function init(el, binding) {
  let position = binding.arg || 'top'
  let content = binding.value || 'Tooltip text'
  el.setAttribute('position', position)
  el.setAttribute('tooltip', content)
}

export default tooltipDirective

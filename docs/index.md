---
layout: home

hero:
  name: Onekind
  text: Design System
  tagline: Build with ❤️ Vue3 and Vite
  image:
    src: /logo-square.png
    alt: Onekind Design System
  actions:
    - theme: brand
      text: Get Started
      link: /guide/
    - theme: alt
      text: View on Gitlab
      link: https://gitlab.com/onekind/design-system

features:
  - icon: ⚡️
    title: Build with Vue3
    details: Using composition API
  - icon: 😎
    title: Minimalistic
    details: Each component is responsible for one feature only
  - icon: 📐
    title: Responsive
    details: Components adapt to the defined breakpoints
---

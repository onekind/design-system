import { defineConfig } from 'vite'
import { resolve } from 'path'
import { VitePluginFonts } from 'vite-plugin-fonts'
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons'

// import svgLoader from 'vite-svg-loader'

export default defineConfig({
  plugins: [
    VitePluginFonts({
      google: {
        families: [
          {
            name: 'IBM+Plex+Sans',
            styles: 'ital,wght@300;400;600',
            defer: true,
          },
          {
            name: 'IBM+Plex+Serif',
            styles: 'ital,wght@300;400;600',
            defer: true,
          },
          {
            name: 'IBM+Plex+Sans+Condensed',
            styles: 'wght@300;400;600',
            defer: true,
          },
          {
            name: 'IBM+Plex+Mono',
            styles: 'wght@300;400;600',
            defer: true,
          },
        ],
      },
    }),
    createSvgIconsPlugin({
      // Specify the icon folder to be cached
      iconDirs: [resolve(process.cwd(), 'src/assets/icons')],
      // Specify symbolId format
      symbolId: 'icon-[dir]-[name]',

      /**
       * custom insert position
       * @default: body-last
       */
      inject: 'body-last', // | 'body-first'

      /**
       * custom dom id
       * @default: __svg__icons__dom__
       */
      customDomId: '__svg__icons__dom__',
    }),
  ],
  css: {
    preprocessorOptions: {
      scss: {
        charset: false,
        additionalData: `
          @import "../src/styles/variables";
          @import "../src/styles/theme";
          @import "../src/styles/grid";
        `,
      },
    },
  },
})

// import '@ibm/plex/scss/ibm-plex.scss'

import DefaultTheme from 'vitepress/theme'
// import '../../../src/styles/index.scss'
import Components from '../../../src/'
import Sample from '../../components/Sample.vue'
import Color from '../../components/Color.vue'
import Wrapper from '../../components/Wrapper.vue'
import './theme.scss'
import 'virtual:svg-icons-register'


// import '../../../src/styles/styleguide.scss'

export default {
  ...DefaultTheme,
  enhanceApp({ app }) {
    app.component('Sample', Sample)
    app.component('Wrapper', Wrapper)
    app.component('Color', Color)
    app.use(Components)
  },
}

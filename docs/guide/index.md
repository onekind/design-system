# Get Started

## Installing the component

```bash
yarn add @onekind/design-system
```

## Importing

### Component import

Add all the components, including the external ones to your main project file.

```js
import OnekindDesignSystem from '@onekind/design-system'

app.use(OnekindDesignSystem)
```

## Options

There are a few options to customize how the component behaves:

- `title`: an optional title string
- `allowEdit`: a boolean flag (`false` by default) to allow the component to edit the model data
- `dark`: dark mode flag (`false` by default)
- `startCollapsed`: `false` by default, allows users to ensure the component starts with only the first line displayed

## Customizing

There are several CSS custom properties which allow configuration of the component, these are:

* `--design-system-color-border-hover`: the color of the border when the user hovers an editable line
* `--design-system-color-dimmed`: the color for pseudo values such as `null`
* `--design-system-color-editor-background`: the color for the input field when `allowEdit` attribute is `true`
* `--design-system-color-type`: the color for the `type` representation (i.e: `{Object}`)
* `--design-system-font-family`: the font family for the main content
* `--design-system-font-size`: the base font size for the component
* `--design-system-font-weight`: the font weight for the main content
* `--design-system-ms-font-family`: the font family for all monospaced elements (i.e.: the `type`)
* `--design-system-ms-font-weight`: the font weight for all monospaced elements
* `--design-system-primary-dark`: the primary color for when the `dark` attribute is set
* `--design-system-primary`: the primary color
* `--design-system-sprite-dark`: the sprite used for the dark version tree decoration
* `--design-system-sprite-light`: the sprite used for the light version tree decoration

> both sprites are available in the `src/assets` folder for customization. they must be converted to `base64` when used.

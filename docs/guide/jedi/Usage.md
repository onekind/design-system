# Usage

## Default

> this example will display test data

<Sample title="Default">
  <JediBasic />
</Sample>

<<< @/guide/jedi/examples/basic.vue

## Dark and Collapsed

> different theme for dark mode (manually toggled), also notice it's collapsed by default

<Sample title="Dark and Collapsed">
  <JediDarkAndCollapsed />
</Sample>

<<< @/guide/jedi/examples/dark-and-collapsed.vue

## Editable

<Sample title="Editable" column>
  <JediEditable />
</Sample>

<<< @/guide/jedi/examples/editable.vue

## Complex

<JediComplex />

<<< @/guide/jedi/examples/complex.vue

<script setup>
import JediBasic from './examples/basic.vue'
import JediDarkAndCollapsed from './examples/dark-and-collapsed.vue'
import JediEditable from './examples/editable.vue'
import JediComplex from './examples/complex.vue'
</script>

# Colors

<div class="sample-grid">
  <Color flavour="primary" />
  <Color flavour="accent" />
  <Color flavour="grey" />
  <Color flavour="green" />
  <Color flavour="yellow" />
  <Color flavour="red" />
</div>

<style lang="scss">
.sample-grid {
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-auto-rows: auto;
  grid-column-gap: var(--grid-base);
  grid-row-gap: calc(var(--grid-base) * 2);
}
</style>

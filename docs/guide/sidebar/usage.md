# Usage

## Default

<Sample title="Default">
  <SidebarBasic />
</Sample>

<<< @/guide/sidebar/examples/basic.vue

## Blocked

<Sample title="Default">
  <SidebarBlocked />
</Sample>

<<< @/guide/sidebar/examples/blocked.vue

## Locked

<Sample title="Default">
  <SidebarLocked />
</Sample>

<<< @/guide/sidebar/examples/locked.vue

<script setup>
import SidebarBasic from './examples/basic.vue'
import SidebarBlocked from './examples/blocked.vue'
import SidebarLocked from './examples/locked.vue'
</script>

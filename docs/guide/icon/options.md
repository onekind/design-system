# Options

## Color

<Sample title="color">
  <IconColor />
</Sample>

<<< @/guide/icon/examples/color.vue

## Size

<Sample title="size">
  <IconSize />
</Sample>

<<< @/guide/icon/examples/size.vue

<script setup>
import IconColor from './examples/color.vue'
import IconSize from './examples/size.vue'
</script>

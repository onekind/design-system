# Usage

## Line Icons

> this example will display the exit icon from lineicons

<Sample title="provider='lineicons'">
  <IconLineIcons />
</Sample>

<<< @/guide/icon/examples/lineicons.vue

<script setup>
import IconLineIcons from './examples/lineicons.vue'
</script>

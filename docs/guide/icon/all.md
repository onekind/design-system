# Usage

## Feather Icons (default)

> this example will display the `feather` icon

<Sample title="all">
  <IconAll />
</Sample>

<<< @/guide/icon/examples/all.vue

<script setup>
import IconAll from './examples/all.vue'
</script>

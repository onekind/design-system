# Usage

## Default

<Sample title="Default">
  <ContentListBasic />
</Sample>

<<< @/guide/contentlist/examples/basic.vue

<script setup>
import ContentListBasic from './examples/basic.vue'
</script>

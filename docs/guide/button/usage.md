# Usage

## Default

<Sample title="Flavour">
  <ButtonBasic />
</Sample>

## Size

<Sample title="Size">
  <ButtonSize />
</Sample>

<<< @/guide/button/examples/basic.vue

<script setup>
import ButtonBasic from './examples/basic.vue'
import ButtonSize from './examples/size.vue'
</script>

# Usage

## Compact

<Sample title="Collapsible">
  <SidenavCompact />
</Sample>

<<< @/guide/sidenav/examples/compact.vue

<script setup>
import SidenavCompact from './examples/compact.vue'
</script>

<style>
  .stage {
    position: relative;
    min-height: 500px;
  }
</style>

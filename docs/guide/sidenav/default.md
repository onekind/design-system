# Usage

## Default

<Sample title="Default">
  <SidenavBasic />
</Sample>

<<< @/guide/sidenav/examples/basic.vue

<script setup>
import SidenavBasic from './examples/basic.vue'
</script>

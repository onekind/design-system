# Vertical alignment

## basic

<Wrapper>
  <VerticalAlignmentBasic />
</Wrapper>

<<< @/guide/grid/examples/vertical-alignment-basic.vue{1,4,7}

## responsive

<Wrapper>
  <VerticalAlignmentResponsive />
</Wrapper>

<<< @/guide/grid/examples/vertical-alignment-responsive.vue{1}

<script setup>
import VerticalAlignmentBasic from './examples/vertical-alignment-basic.vue'
import VerticalAlignmentResponsive from './examples/vertical-alignment-responsive.vue'
</script>

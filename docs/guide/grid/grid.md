# Grid

The maximum number of columns is by default defined as `16`

## basic

<Wrapper>
  <GridBasic />
</Wrapper>

<<< @/guide/grid/examples/grid-basic.vue{2,5-6,9-10,13-14,17-18,21-22,25-26}

## responsive

<Wrapper>
  <GridResponsive />
</Wrapper>

<<< @/guide/grid/examples/grid-responsive.vue

## shift

<Wrapper>
  <GridShift />
</Wrapper>

<<< @/guide/grid/examples/grid-shift.vue

<script setup>
import GridBasic from './examples/grid-basic.vue'
import GridResponsive from './examples/grid-responsive.vue'
import GridShift from './examples/grid-shift.vue'
</script>

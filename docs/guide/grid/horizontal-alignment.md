# Horizontal alignment

## basic

<Wrapper>
  <HorizontalAlignmentBasic />
</Wrapper>

<<< @/guide/grid/examples/horizontal-alignment-basic.vue{2,5,8,11,16,21}

## responsive

<Wrapper>
  <HorizontalAlignmentResponsive />
</Wrapper>

<<< @/guide/grid/examples/horizontal-alignment-responsive.vue{2,5,8}

<script setup>
import HorizontalAlignmentBasic from './examples/horizontal-alignment-basic.vue'
import HorizontalAlignmentResponsive from './examples/horizontal-alignment-responsive.vue'
</script>

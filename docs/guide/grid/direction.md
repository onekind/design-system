# Direction

## basic

<Wrapper>
  <DirectionBasic />
</Wrapper>

<<< @/guide/grid/examples/direction-basic.vue{3}

## Responsive

<Wrapper>
  <DirectionResponsive />
</Wrapper>

<<< @/guide/grid/examples/direction-responsive.vue{1}

## Mixed responsive

<Wrapper>
  <DirectionMixedResponsive />
</Wrapper>

<<< @/guide/grid/examples/direction-mixed-responsive.vue{1}

<script setup>
import DirectionBasic from './examples/direction-basic.vue'
import DirectionResponsive from './examples/direction-responsive.vue'
import DirectionMixedResponsive from './examples/direction-mixed-responsive.vue'
</script>

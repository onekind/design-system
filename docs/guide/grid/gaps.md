# Gaps

## basic

<Wrapper>
  <GapsBasicRow />
</Wrapper>

<<< @/guide/grid/examples/gaps-basic-row.vue{1,5,9,13,17}

<Wrapper>
  <GapsBasicCol />
</Wrapper>

<<< @/guide/grid/examples/gaps-basic-col.vue{1,5,9,13,17}

## Responsive

<Wrapper>
  <GapsResponsive />
</Wrapper>

<<< @/guide/grid/examples/gaps-responsive.vue{1,5}

## Modes

<Wrapper>
  <GapsModes />
</Wrapper>

<<< @/guide/grid/examples/gaps-modes.vue{1,5}

<script setup>
import GapsBasicRow from './examples/gaps-basic-row.vue'
import GapsBasicCol from './examples/gaps-basic-col.vue'
import GapsResponsive from './examples/gaps-responsive.vue'
import GapsModes from './examples/gaps-modes.vue'
</script>

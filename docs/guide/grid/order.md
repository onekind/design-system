# Order

## basic `reverse`

<Wrapper>
  <OrderBasicReverse />
</Wrapper>

<<< @/guide/grid/examples/order-reverse.vue{1}

## responsive `reverse`

<Wrapper>
  <OrderResponsiveReverse />
</Wrapper>

<<< @/guide/grid/examples/order-responsive-reverse.vue{1}

## basic `first`

<Wrapper>
  <OrderBasicFirst />
</Wrapper>

<<< @/guide/grid/examples/order-first.vue{4}

## basic `last`

<Wrapper>
  <OrderBasicLast />
</Wrapper>

<<< @/guide/grid/examples/order-last.vue{3}

## responsive `first` `last`

<Wrapper>
  <OrderResponsiveFirstLast />
</Wrapper>

<<< @/guide/grid/examples/order-responsive-first-last.vue{3}

<script setup>
import OrderBasicReverse from './examples/order-reverse.vue'
import OrderResponsiveReverse from './examples/order-responsive-reverse.vue'
import OrderBasicFirst from './examples/order-first.vue'
import OrderBasicLast from './examples/order-last.vue'
import OrderResponsiveFirstLast from './examples/order-responsive-first-last.vue'
</script>

# Wrap

## basic

<Wrapper>
  <WrapBasic />
</Wrapper>

<<< @/guide/grid/examples/wrap-basic.vue{1,13,25}

## responsive

<Wrapper>
  <WrapResponsive />
</Wrapper>

<<< @/guide/grid/examples/wrap-responsive.vue{1}

<script setup>
import WrapBasic from './examples/wrap-basic.vue'
import WrapResponsive from './examples/wrap-responsive.vue'
</script>

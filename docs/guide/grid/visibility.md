# Visibiliy

## basic

<Wrapper>
  <VisibilityBasic />
</Wrapper>

<<< @/guide/grid/examples/visibility-basic.vue{2}

## responsive

<Wrapper>
  <VisibilityResponsive />
</Wrapper>

<<< @/guide/grid/examples/visibility-responsive.vue{2}

<script setup>
import VisibilityBasic from './examples/visibility-basic.vue'
import VisibilityResponsive from './examples/visibility-responsive.vue'
</script>

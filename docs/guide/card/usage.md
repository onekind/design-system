# Usage

## Default

<Sample flex title="Default">
  <CardBasic />
</Sample>

<<< @/guide/card/examples/basic.vue

## Clickable

<Sample flex title="Default">
  <CardClickable />
</Sample>

<<< @/guide/card/examples/clickable.vue

## With header image

<Sample flex title="Default">
  <CardImage />
</Sample>

<<< @/guide/card/examples/image.vue

## As line

<Sample flex title="Default" column>
  <CardAsLine />
</Sample>

<<< @/guide/card/examples/asline.vue

<script setup>
import CardBasic from './examples/basic.vue'
import CardClickable from './examples/clickable.vue'
import CardImage from './examples/image.vue'
import CardAsLine from './examples/asline.vue'
</script>

# @onekind/design-system

![logo](https://gitlab.com/onekind/design-system/raw/master/docs/public/logo.svg)

[![build status](https://img.shields.io/gitlab/pipeline/onekind/design-system/master.svg?style=for-the-badge)](https://gitlab.com/onekind/design-system.git)
[![npm-publish](https://img.shields.io/npm/dm/@onekind/design-system.svg?style=for-the-badge)](https://www.npmjs.com/package/@onekind/design-system)
[![release](https://img.shields.io/npm/v/@onekind/design-system?label=%40onekind%2Fdesign-system%40latest&style=for-the-badge)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg?style=for-the-badge)](https://github.com/semantic-release/semantic-release)

Design system for Onekind apps, strongly inspired by the IBM Carbon Design System

Checkout the [Demo](https://onekind.gitlab.io/design-system/) which contains the component documentation.

> If you enjoy this component, feel free to drop me feedback, either via the repository, or via jose@onekind.io.

## Instalation

```bash
yarn add @onekind/design-system
```

## Setup

### Vue

- Add the following to you application main.js file:

```js
import {OnekindDesignSystem} from '@onekind/design-system'

app.use(OnekindDesignSystem)
```

## reference

- [coolors](https://coolors.co/2d7dd2-643a71-99d5c9-6da84d-f7b267-fb3640-f0e2bb-84673d-77989a-2a2939-f4f4f4-9b9b9b-c4c4c4-393939)


- https://kalimah-apps.github.io/vue-icons/

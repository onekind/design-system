import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'
import { defineConfig } from 'vite'
import { visualizer } from 'rollup-plugin-visualizer'
import { VitePluginFonts } from 'vite-plugin-fonts'
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    // https://github.com/btd/rollup-plugin-visualizer
    {...(process.env.ANALYZE && visualizer({
      open: true,
      title: 'Onekind Design System',
      template: process.env.ANALYZETEMPLATE || 'treemap',
      gzipSize: true,
    }))},
    VitePluginFonts({
      google: {
        families: [
          {
            name: 'IBM+Plex+Sans',
            styles: 'ital,wght@300;400;600',
            defer: true,
          },
          {
            name: 'IBM+Plex+Serif',
            styles: 'ital,wght@300;400;600',
            defer: true,
          },
          {
            name: 'IBM+Plex+Sans+Condensed',
            styles: 'wght@300;400;600',
            defer: true,
          },
          {
            name: 'IBM+Plex+Mono',
            styles: 'wght@300;400;600',
            defer: true,
          },
        ],
      },
    }),
    createSvgIconsPlugin({
      // Specify the icon folder to be cached
      iconDirs: [resolve(process.cwd(), 'src/assets/icons')],
      // Specify symbolId format
      symbolId: 'icon-[dir]-[name]',

      /**
       * custom insert position
       * @default: body-last
       */
      inject: 'body-last', // | 'body-first'

      /**
       * custom dom id
       * @default: __svg__icons__dom__
       */
      customDomId: '__svg__icons__dom__',
    }),
  ],
  resolve: {
    alias: {
      '~': resolve(__dirname, 'src'),
      vue: resolve('./node_modules/vue'),
    },
    symlinks: false,
  },
  css: {
    preprocessorOptions: {
      scss: {
        charset: false,
        // additionalData: `@import "~/styles";`,
        additionalData: `@import "~/styles/variables";`,
      },
    },
  },
  build: {
    lib: {
      entry: resolve(__dirname, 'src/index.js'),
      fileName: (format) => `design-system.${format}.js`,
      // formats: ['es'],
      name: 'design-system',
    },
    rollupOptions: {
      // make sure to externalize deps that shouldn't be bundled
      // into your library
      external: ['vue'],
      output: {
        // Provide global variables to use in the UMD build
        // for externalized deps
        globals: {
          vue: 'Vue',
        },
        assetFileNames: (assetInfo) => {
          if (assetInfo.name == 'style.css')
            return 'design-system.css'
          return assetInfo.name
        },
      },
    },
  },
})

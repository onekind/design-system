# [1.0.0-beta.37](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.36...v1.0.0-beta.37) (2022-10-31)


### Bug Fixes

* update dependencies version ([9e8ea33](https://gitlab.com/onekind/design-system/commit/9e8ea33cfe0e0d3a86ae26d821ae520845013c43))

# [1.0.0-beta.36](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.35...v1.0.0-beta.36) (2022-10-31)


### Bug Fixes

* improvements to various componens and color system ([6e6c518](https://gitlab.com/onekind/design-system/commit/6e6c51869757603c990ef8e44e9c141038237a9a))

# [1.0.0-beta.35](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.34...v1.0.0-beta.35) (2022-10-29)


### Bug Fixes

* use math in sass ([44fc1ac](https://gitlab.com/onekind/design-system/commit/44fc1ac1f6e4a2b5a06d64c03e6769efb5507993))

# [1.0.0-beta.34](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.33...v1.0.0-beta.34) (2022-10-03)


### Features

* new form and input ([534ac14](https://gitlab.com/onekind/design-system/commit/534ac147fc5c7cfbda6b2e45c227434cc6566979))

# [1.0.0-beta.33](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.32...v1.0.0-beta.33) (2022-09-26)


### Bug Fixes

* ci not allowing automated releases ([8ce0709](https://gitlab.com/onekind/design-system/commit/8ce070918cf88f18e58dadb15b14c37889a8f134))

# [1.0.0-beta.32](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.31...v1.0.0-beta.32) (2022-09-26)


### Features

* add search for all icons filtering ([b1d03ec](https://gitlab.com/onekind/design-system/commit/b1d03ecc5a6eaaf4996e3d2f6e751d0ce35d89d0))

# [1.0.0-beta.31](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.30...v1.0.0-beta.31) (2022-09-25)


### Features

* add tour component ([d6d4d5b](https://gitlab.com/onekind/design-system/commit/d6d4d5b3886382ce48de43b542c2edfe50d857c3))

# [1.0.0-beta.30](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.29...v1.0.0-beta.30) (2022-09-24)


### Bug Fixes

* lint command ([dbc90d8](https://gitlab.com/onekind/design-system/commit/dbc90d8f45a53f9ba8b2d47bbddb67f91077defa))

# [1.0.0-beta.29](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.28...v1.0.0-beta.29) (2022-09-24)


### Bug Fixes

* CI not mentioning node image ([088cfab](https://gitlab.com/onekind/design-system/commit/088cfab0dc7f867320101410bc937ccffef43c8c))
* ContentList not spreading the full width ([669a847](https://gitlab.com/onekind/design-system/commit/669a847d959384bea5e2d3912c19eb7328a49fa7))
* pnpm setup ([0d00426](https://gitlab.com/onekind/design-system/commit/0d00426f1bed9932b970615804627de380656ba1))
* pnpm setup in CI ([430a4a6](https://gitlab.com/onekind/design-system/commit/430a4a6b9bfa5bbeeaf754c32d238c31b48d4d56))
* pnpm setup in gitlab. CI ([558960c](https://gitlab.com/onekind/design-system/commit/558960c2e3fc7f69733c6e329b9241bdd7560880))
* replace yarn with pnpm ([b018930](https://gitlab.com/onekind/design-system/commit/b018930363015dbe5ef15683e56f6690e2ff7fb5))


### Features

* add the categories icon ([e5a07dd](https://gitlab.com/onekind/design-system/commit/e5a07dd4001a279876dcf3b3247eb4e429003b3a))
* sidebar and sidenav ([b25cf0e](https://gitlab.com/onekind/design-system/commit/b25cf0ed3aacfc82de11a7ec03a44ca9375820a0))

# [1.0.0-beta.28](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.27...v1.0.0-beta.28) (2022-09-22)


### Bug Fixes

* content list issues ([da6036a](https://gitlab.com/onekind/design-system/commit/da6036abaf8c24319141cf8cfdbbd0cf3df35ac1))

# [1.0.0-beta.27](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.26...v1.0.0-beta.27) (2022-09-22)


### Bug Fixes

* add deep to card styling ([d2e45bb](https://gitlab.com/onekind/design-system/commit/d2e45bb16feefa24a693701f8ece4f4f3d7c983e))


### Features

* add content list component ([bc63399](https://gitlab.com/onekind/design-system/commit/bc63399d3654fc50e4607b80b58c08361db4c741))

# [1.0.0-beta.26](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.25...v1.0.0-beta.26) (2022-09-21)


### Bug Fixes

* button disabled logic ([ce7921e](https://gitlab.com/onekind/design-system/commit/ce7921e0b93033e162a0b627046e4b014ab8784c))

# [1.0.0-beta.25](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.24...v1.0.0-beta.25) (2022-09-21)


### Bug Fixes

* allow sidebar to lock ([ec0b0b2](https://gitlab.com/onekind/design-system/commit/ec0b0b242635940ff2a4fd522963acf48fd8b0e5))

# [1.0.0-beta.24](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.23...v1.0.0-beta.24) (2022-09-21)


### Bug Fixes

* inspector ([ea2c647](https://gitlab.com/onekind/design-system/commit/ea2c6470fb301f74d96a4496d791dfa8516406a2))
* inspector again ([8c1e3fe](https://gitlab.com/onekind/design-system/commit/8c1e3fe0dd3782b60e7fbaf7e3bb2e582845bb63))

# [1.0.0-beta.23](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.22...v1.0.0-beta.23) (2022-09-21)


### Features

* add inspector ([e939e65](https://gitlab.com/onekind/design-system/commit/e939e65005b173fc9a2e6f94588bcf2c4c96825a))

# [1.0.0-beta.22](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.21...v1.0.0-beta.22) (2022-09-20)


### Bug Fixes

* issue in sidebar no expanding links ([b213ce1](https://gitlab.com/onekind/design-system/commit/b213ce1a24ee8ff09d92bf3df25f28ab6c0b6a16))

# [1.0.0-beta.21](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.20...v1.0.0-beta.21) (2022-09-19)


### Bug Fixes

* what didn't I fix? ([b940cf7](https://gitlab.com/onekind/design-system/commit/b940cf7353f43d142a0c02c8f01845a4a64d5305))

# [1.0.0-beta.20](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.19...v1.0.0-beta.20) (2022-09-18)


### Bug Fixes

* fix and more fix ([40e6875](https://gitlab.com/onekind/design-system/commit/40e6875bdd7c6e3305788b5262b32cb6864c292b))

# [1.0.0-beta.19](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.18...v1.0.0-beta.19) (2022-09-18)


### Features

* add CardMode component ([84e1265](https://gitlab.com/onekind/design-system/commit/84e1265f55ad905c7a2d8c71260523fea496cff9))

# [1.0.0-beta.18](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.17...v1.0.0-beta.18) (2022-09-18)


### Bug Fixes

* temprarily change primary color for ease ([881c918](https://gitlab.com/onekind/design-system/commit/881c918cea4a8c753659d0a6e31ad63cedab47a5))

# [1.0.0-beta.17](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.16...v1.0.0-beta.17) (2022-09-18)


### Bug Fixes

* remove console.logs ([2043fcc](https://gitlab.com/onekind/design-system/commit/2043fcc0f3c56618990188e8f177950fe1926ee4))

# [1.0.0-beta.16](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.15...v1.0.0-beta.16) (2022-09-18)


### Bug Fixes

* add a new size to icons ([1632335](https://gitlab.com/onekind/design-system/commit/163233579f484d18790a894b5089d3c868363824))

# [1.0.0-beta.15](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.14...v1.0.0-beta.15) (2022-09-18)


### Bug Fixes

* wrong color on hover for compact sidebar ([1e94711](https://gitlab.com/onekind/design-system/commit/1e94711acdbd9c10d3586d8d358ea7e2ccb53b09))

# [1.0.0-beta.14](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.13...v1.0.0-beta.14) (2022-09-18)


### Bug Fixes

* removed dummy stuff from tabs component example ([2bf7903](https://gitlab.com/onekind/design-system/commit/2bf7903ceda573e55c71fbfa3839f6a993bc6b4e))

# [1.0.0-beta.13](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.12...v1.0.0-beta.13) (2022-09-18)


### Bug Fixes

* remove component index files which were breaking vite watch ([3a15e8e](https://gitlab.com/onekind/design-system/commit/3a15e8edb30df8367f2caa8b56a7d9adf8ed758e))


### Features

* add tabs component ([4eea1dc](https://gitlab.com/onekind/design-system/commit/4eea1dcacef67450070074bee0cd4fc6ca2248e4))

# [1.0.0-beta.12](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.11...v1.0.0-beta.12) (2022-09-17)


### Bug Fixes

* tons of bugfixes ([b8120ff](https://gitlab.com/onekind/design-system/commit/b8120ff6e5434636b9ee501c516ccfe2b87ff808))

# [1.0.0-beta.11](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.10...v1.0.0-beta.11) (2022-09-17)


### Bug Fixes

* icons sizes ([e6e1017](https://gitlab.com/onekind/design-system/commit/e6e1017f4306c66ade15432a332e0e54485f5054))

# [1.0.0-beta.10](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.9...v1.0.0-beta.10) (2022-09-17)


### Features

* new icons, bug fixes and overall improvements ([6845e79](https://gitlab.com/onekind/design-system/commit/6845e79ec4633015b81a7962eda37870b46ee49f))

# [1.0.0-beta.9](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.8...v1.0.0-beta.9) (2022-09-16)


### Bug Fixes

* refactor menu to be a recursive single component ([d1ba642](https://gitlab.com/onekind/design-system/commit/d1ba6420117c726f5c61c24bbac29f7488b2fd73))

# [1.0.0-beta.8](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.7...v1.0.0-beta.8) (2022-09-15)


### Features

* move menus to design-system ([4f4e843](https://gitlab.com/onekind/design-system/commit/4f4e8438c4843c8f8da6706e0303fae6a52f1c2c))

# [1.0.0-beta.7](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.6...v1.0.0-beta.7) (2022-09-15)


### Bug Fixes

* change card to handle click instead of navigation ([3b43dcb](https://gitlab.com/onekind/design-system/commit/3b43dcbe063f67b200ad8d97e55bb231c428b928))

# [1.0.0-beta.6](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.5...v1.0.0-beta.6) (2022-09-15)


### Features

* add card sample to docs ([a2d79bc](https://gitlab.com/onekind/design-system/commit/a2d79bcc5faeebb95403a57249aa15ecaf0e294a))

# [1.0.0-beta.5](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.4...v1.0.0-beta.5) (2022-09-15)


### Bug Fixes

* make guides standalone ([8ea83b1](https://gitlab.com/onekind/design-system/commit/8ea83b1139e0624ea77cf7fe617b38a7aa195761))
* remove dependency of grid values in guides ([85845d6](https://gitlab.com/onekind/design-system/commit/85845d6f8f6073da343594e54a14a0d8f08dffdf))

# [1.0.0-beta.4](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.3...v1.0.0-beta.4) (2022-09-15)


### Bug Fixes

* use vite-fonts-plugin to import fonts ([4df554a](https://gitlab.com/onekind/design-system/commit/4df554a2b69c2adb0f8343dad56d47c1ab19cb45))

# [1.0.0-beta.3](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.2...v1.0.0-beta.3) (2022-09-15)


### Bug Fixes

* namespace all components ([c97d0a5](https://gitlab.com/onekind/design-system/commit/c97d0a5ec48368ec5c08ea59bb8c443d5227cd80))


### Features

* add dynamic media queries ([07d73dd](https://gitlab.com/onekind/design-system/commit/07d73dd21941b611a32d39eeba14cebd9caffa8d))

# [1.0.0-beta.2](https://gitlab.com/onekind/design-system/compare/v1.0.0-beta.1...v1.0.0-beta.2) (2022-09-14)


### Bug Fixes

* enforce release of src folder ([d0109d3](https://gitlab.com/onekind/design-system/commit/d0109d3d94113ccc6094fce8bf579f553052b82c))

# 1.0.0-beta.1 (2022-09-14)


### Bug Fixes

* add docs build to the other release branches ([9416db4](https://gitlab.com/onekind/design-system/commit/9416db4ca1e869e88eb0d6eda1d86d36c2f3e1f1))
* implemented remaining components ([fe42857](https://gitlab.com/onekind/design-system/commit/fe4285725ebb5fee72627f6436d9dc08fc3c99ff))
* no more sass variables in jedi ([8532ce1](https://gitlab.com/onekind/design-system/commit/8532ce14e11e1d4f8b00e9036a06accc3dcb4d47))
* release branches ([7ed8989](https://gitlab.com/onekind/design-system/commit/7ed89898348965dde7b43dad45472ac30741407a))
* remaining mistakes regarding scss variables ([248597e](https://gitlab.com/onekind/design-system/commit/248597e55d1e655cf17cfff0c512947205082137))
* remove sass variables from components ([d6a0434](https://gitlab.com/onekind/design-system/commit/d6a04341cf21b813e985b3c2ab5926458c03834b))
